<?php
require '../model/loginModel.php';
$Login = new Login();

if(isset($_POST['login'])){
	$email = trim(htmlentities($_POST['email']));
	$password = trim(htmlentities($_POST['password']));
	if($email == "admin@admin" && $password=="admin"){
		$_SESSION['user_id'] = "admin";
		$_SESSION['user_type']= "admin";
		header("location:../view/admin_dashboard.php?message=admin");
	}
	 if($email != "" && $password !=""){
		$Login->login($email,$password);
		//after loggng in proceed to dashboard
		if($Login == true){
			$link = ($_SESSION['user_type']=="meatshop") ? "meatshop_dashboard" : "consumer_dashboard";
			header("location:../view/".$link.".php?message=welcome");
		}else{
			header("location:../login.php?message=wrong_credentials");
		}
	}
	else{
		header("location:../login.php?message=invalid");
	}
}