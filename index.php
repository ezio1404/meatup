<?php


?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/navbar-top-fixed.css">
    <link rel="stylesheet" href="assets/css/sticky-footer.css">

    <title>MeatUp</title>
</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: maroon !important;">
        <a class="navbar-brand" href="#">
            <img src="image/meatup_logo2.png" width="30" height="30" class="d-inline-block align-top" alt="">
            MeatUp
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
            </ul>
            <span class="navbar-text">
                <a href="login.php?message=login">Login</a>
            </span>
        </div>
    </nav>

    <main role="main" class="container">

    </main>


    <footer class="footer">
        <div class="container">
            <span class="text">Copyright © 2019 Meatup All rights reserved. </span>
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Bootstrap Core JS -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>