-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2019 at 12:52 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meatup_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consumer`
--

CREATE TABLE `tbl_consumer` (
  `consumer_id` int(11) NOT NULL,
  `consumer_fname` varchar(255) NOT NULL,
  `consumer_lname` varchar(255) NOT NULL,
  `consumer_mi` varchar(255) NOT NULL,
  `consumer_email` varchar(255) NOT NULL,
  `consumer_password` varchar(255) NOT NULL,
  `consumer_address` varchar(255) NOT NULL,
  `consumer_contact` varchar(255) NOT NULL,
  `consumer_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `consumer_status` varchar(255) NOT NULL DEFAULT 'active'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_consumer`
--

INSERT INTO `tbl_consumer` (`consumer_id`, `consumer_fname`, `consumer_lname`, `consumer_mi`, `consumer_email`, `consumer_password`, `consumer_address`, `consumer_contact`, `consumer_dateCreated`, `consumer_status`) VALUES
(1, 'asdasd', 'sdasdas', 'a', 'test', 'test', 'asdasdasd', '123123123', '2019-08-17 14:53:15', 'active'),
(2, 'test', 'test', 'test', 'test@test', 'test', 'test', '123123123', '2019-08-17 14:58:20', 'active'),
(3, 'asdasd', 'sdasdas', 'a', 'asdas@Asdasdas', 'asd123', 'asdasdasdasdas', '123123123', '2019-08-17 15:01:56', 'active'),
(4, 'asdsaads', 'dasd', 'sadasd', 'asdasd@asdasd', '123', 'ads', '1232131asd', '2019-08-30 22:46:02', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_meatshop`
--

CREATE TABLE `tbl_meatshop` (
  `ms_id` int(11) NOT NULL,
  `ms_profileRef` varchar(255) NOT NULL,
  `ms_companyname` varchar(255) NOT NULL,
  `ms_email` varchar(255) NOT NULL,
  `ms_password` varchar(255) NOT NULL,
  `ms_address` varchar(255) NOT NULL,
  `ms_contact` varchar(255) NOT NULL,
  `ms_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ms_status` varchar(255) NOT NULL DEFAULT 'active'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_meatshop`
--

INSERT INTO `tbl_meatshop` (`ms_id`, `ms_profileRef`, `ms_companyname`, `ms_email`, `ms_password`, `ms_address`, `ms_contact`, `ms_dateCreated`, `ms_status`) VALUES
(1, 'meatup_logo1.png', 'VIginaia', 'asd@asd', 'asd', 'asdasda', '123123sad', '2019-08-17 16:00:00', 'active'),
(2, 'meatup_logo2.png', 'company name', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2019-08-30 21:52:37', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `prod_id` int(11) NOT NULL,
  `ms_id` int(11) NOT NULL,
  `prod_category` varchar(255) NOT NULL,
  `prod_name` varchar(255) NOT NULL,
  `prod_stock` int(11) NOT NULL,
  `prod_company` varchar(255) NOT NULL,
  `prod_price` double(10,2) NOT NULL,
  `prod_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prod_updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prod_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_consumer`
--
ALTER TABLE `tbl_consumer`
  ADD PRIMARY KEY (`consumer_id`);

--
-- Indexes for table `tbl_meatshop`
--
ALTER TABLE `tbl_meatshop`
  ADD PRIMARY KEY (`ms_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`prod_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_consumer`
--
ALTER TABLE `tbl_consumer`
  MODIFY `consumer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_meatshop`
--
ALTER TABLE `tbl_meatshop`
  MODIFY `ms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
