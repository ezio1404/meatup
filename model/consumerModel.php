<?php
require 'db/dbhelper.php';
Class Consumer extends DBHelper{
    private $table = 'tbl_consumer';
    private $fields = array(
        'consumer_fname',
        'consumer_lname',
        'consumer_mi',
        'consumer_email',
        'consumer_password',
        'consumer_address',
        'consumer_contact'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addConsumer($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllConsumer(){
     return DBHelper::getAllRecord($this->table);
 }
 function getConsumerById($ref_id){
    return DBHelper::getRecordById($this->table,'consumer_id',$ref_id);
}
function getConsumer($ref_id){
    return DBHelper::getRecord($this->table,'consumer_id',$ref_id);
}
// Update
function updateConsumer($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->fields,$data,'consumer_id',$ref_id); 
 }
 // Delete
 function deleteConsumer($ref_id){
          return DBHelper::deleteRecord($this->table,'consumer_id',$ref_id);
}
// Some Functions
    function getCountConsumer(){
        return DBHelper::countRecord('consumer_id',$this->table);
    }
    function getFields(){
        return $this->fields;
    }
    function getProcConsumer(){
        return DBHelper::getProcedure($this->table);
    }
    function getAllLog(){
        return DBHelper::getAllRecord("tbl_log");
    }
}
?>