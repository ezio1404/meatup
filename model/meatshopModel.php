<?php
require 'db/dbhelper.php';
Class Meatshop extends DBHelper{
    private $table = 'tbl_meatshop';
    private $fields = array(
        'ms_companyname',
        'ms_email',
        'ms_password',
        'ms_address',
        'ms_contact'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addMeatshop($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllMeatshop(){
     return DBHelper::getAllRecord($this->table);
 }
 function getMeatshopById($ref_id){
    return DBHelper::getRecordById($this->table,'ms_id',$ref_id);
}
function getMeatshop($ref_id){
    return DBHelper::getRecord($this->table,'ms_id',$ref_id);
}
// Update
function updateMeatshop($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->fields,$data,'ms_id',$ref_id); 
 }
 // Delete
 function deleteMeatshop($ref_id){
          return DBHelper::deleteRecord($this->table,'ms_id',$ref_id);
}
// Some Functions
    function getCountMeatshop(){
        return DBHelper::countRecord('ms_id',$this->table);
    }
    function getFields(){
        return $this->fields;
    }
    function getProcMeatshop(){
        return DBHelper::getProcedure($this->table);
    }
    function getAllLog(){
        return DBHelper::getAllRecord("tbl_log");
    }
}
?>