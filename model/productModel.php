<?php
require 'db/dbhelper.php';
Class Product extends DBHelper{
    private $table = 'tbl_product';
    private $fields = array(
        'ms_id',
        'prod_photoRef',
        'prod_category',
        'prod_stock',
        'prod_price'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addProduct($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllProduct(){
     return DBHelper::getAllRecord($this->table);
 }
 function getProductById($ref_id){
    return DBHelper::getRecordById($this->table,'prod_id',$ref_id);
}
function getAllProductById($ref_id){
    return DBHelper::getAllProductByMeatshop($ref_id);
}
function getProduct($ref_id){
    return DBHelper::getRecord($this->table,'prod_id',$ref_id);
}
// Update
function updateProduct($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->fields,$data,'prod_id',$ref_id); 
 }
 // Delete
 function deleteProduct($ref_id){
          return DBHelper::deleteRecord($this->table,'prod_id',$ref_id);
}
// Some Functions
    function getCountProduct(){
        return DBHelper::countRecord('prod_id',$this->table);
    }
    function getFields(){
        return $this->fields;
    }
    function getProcProduct(){
        return DBHelper::getProcedure($this->table);
    }

}
?>