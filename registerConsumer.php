<?php

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/navbar-top-fixed.css">
    <link rel="stylesheet" href="assets/css/sticky-footer.css">


    <title>MeatUp</title>

</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top " style="background-color: maroon !important;">
        <a class="navbar-brand" href="index.php">
            <img src="image/meatup_logo2.png" width="30" height="30" class="d-inline-block align-top" alt="logo">
            MeatUp
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
            </ul>
            <span class="navbar-text">
                <a href="login.php?message=">Back</a>
            </span>
        </div>
    </nav>

    <main role="main" class="container">


        <form action="controller/consumerController.php" method="post">
            <div class="form-row">
                <div class="form-group  col-md-12">
                    <!--  -->
                    <?php
                        if($_GET['message']=="invalid"){
                    ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                        }
                    ?>
                    <?php
                        if($_GET['message']=="password_not_matched"){
                    ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Holy guacamole!</strong>Password does not matched.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                        }
                    ?>
                    <?php
                        if($_GET['message']=="error"){
                    ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Holy guacamole!</strong> An error occured
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                        }
                    ?>
                    <!--  -->
                </div>
                <div class=" form-group col-md-12">
                    <h4 class="display-4">Register Consumer</h4>
                </div>
                <div class="form-group  col-md-12">
                    <label for="consumer_fname">First Name</label>
                    <input type="text" required class="form-control" id="consumer_fname" name="consumer_fname"
                        placeholder="John">
                </div>
                <div class="form-group  col-md-12">
                    <label for="consumer_lname">Last name</label>
                    <input type="text" required class="form-control" id="consumer_lname" name="consumer_lname"
                        placeholder="Doe">
                </div>
                <div class="form-group  col-md-12">
                    <label for="consumer_mi">Middle Inital/Name</label>
                    <input type="text" class="form-control" id="consumer_mi" name="consumer_mi"
                        placeholder="D">
                </div>
                <div class="form-group  col-md-12">
                    <label for="consumer_address">Address</label>
                    <input type="text" required class="form-control" id="consumer_address" name="consumer_address"
                        placeholder="Sanciangko St, Cebu City, 6000 Cebu">
                </div>
                <div class="form-group  col-md-12">
                    <label for="consumer_contact">Contact</label>
                    <input type="text" required class="form-control" id="consumer_contact" name="consumer_contact"
                        placeholder="(032) 255 7777">
                </div>

                <div class="form-group  col-md-12">
                    <label for="consumer_email">Email</label>
                    <input type="email" required class="form-control" id="consumer_email" name="consumer_email"
                        placeholder="jd@gmail.com">
                </div>
                <div class="form-group  col-md-12">
                    <label for="consumer_password">Password</label>
                    <input type="password" required class="form-control" id="consumer_password" name="consumer_password" placeholder="******">
                </div>
                <div class="form-group  col-md-12">
                    <label for="retype_password">Retype Password</label>
                    <input type="password" required class="form-control" id="retype_password" name="retype_password" placeholder="******">
                </div>
                <div class="form-group  col-md-12">
                    <input class="btn btn-success" type="submit" value="Register" name="btnSignUpConsumer" id="btnSignUpConsumer">
                </div>
            </div>
        </form>
    </main>

    <footer class="footer">
        <div class="container">
            <span class="text">Copyright © 2019 Meatup All rights reserved. </span>
        </div>
    </footer>

    <!-- MOdal -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Bootstrap Core JS -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>