<?php
include '../model/getAllRecordModel.php';
if(!$_SESSION && $_SESSION['user_type']=="admin"){
    header("location:../login.php?message=pleaseLogin");
}
if($_SESSION['user_type']!="admin"){
    header("location:../login.php?message=pleaseLogin");
}
if($_SESSION['user_type']=="admin"){
    $getAllRecord = new GetAllRecord();
    $consumerList = $getAllRecord->getAllConsumer();
    $meatshopList = $getAllRecord->getAllMeatshop();
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/navbar-top-fixed.css">
    <link rel="stylesheet" href="../assets/css/sticky-footer.css">
    <!-- Datatable -->
    <link rel="stylesheet" href="../assets/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../assets/css/buttons.dataTables.min.css">
    <!-- fontaswesom -->
    <link rel="stylesheet" href="../assets/fontawesome/css/all.css">
    <!-- custom -->
    <link rel="stylesheet" href="../assets/css/custom-style.css">
    <title>MeatUp</title>
    <style>

    </style>
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: maroon !important;">
        <a class="navbar-brand" href="#">
            <img src="../image/meatup_logo2.png" width="30" height="30" class="d-inline-block align-top" alt="">
            MeatUp
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <?php
     if($_SESSION['user_type']=="admin"){
    ?>
                <li class="nav-item active">
                    <a class="nav-link" href="admin_dashboard.php"> <i class="fas fa-columns"></i>
                        Dashboard <span class="sr-only">(current)</span></a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin_reports.php">
                        <i class="fas fa-chart-bar"></i>
                        Reports
                    </a>
                </li>
                <?php
                            }
                        ?>
                <?php
                        if($_SESSION['user_type']=="meatshop"){
                        ?>
                <li class="nav-item">
                    <a class="nav-link active" href="meatshop_dashboard.php">
                        <i class="fas fa-columns"></i>
                        Home <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_orders.php">
                        <i class="fas fa-shopping-cart"></i>
                        Orders
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_products.php">
                        <i class="fas fa-drumstick-bite"></i>
                        Products
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_ratings.php">
                        <i class="fas fa-users"></i>
                        Ratings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_reports.php">
                        <i class="fas fa-chart-bar"></i>
                        Reports
                    </a>
                </li>
            </ul>
            <?php
}
?>
            <?php
                        if($_SESSION['user_type']=="consumer"){
                        ?>
            <li class="nav-item">
                <a class="nav-link " href="consumer_dashboard.php">
                    <i class="fas fa-columns"></i>
                    Home <span class="sr-only">(current)</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link " href="consumer_meatshopList.php">
                    <i class="fas fa-drumstick-bite"></i>
                    Meatshops
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="consumer_cart.php">
                    <i class="fas fa-shopping-cart"></i>
                    Cart<span class="badge badge-pill badge-info">0</span>
                </a>
            </li>
            </ul>
            <?php
}
?>

            <span class="navbar-text">
                <a href="../controller/logout.php">Logout</a>
            </span>
        </div>
    </nav>



            <main class="container-fluid">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Dashboard</h1>
                    <div class="btn-toolbar mb-2 mb-md-0">

                    </div>
                </div>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="consumer-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">Consumer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="meatshop-tab" data-toggle="tab" href="#profile" role="tab"
                            aria-controls="profile" aria-selected="false">Meatshops</a>
                    </li>

                </ul>
                <div class="tab-content " id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="consumer-tab"
                        style="margin-top:1em !important;">
                        <h2>Consumers</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover display nowrap" id="consumerDataTable"
                                style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($consumerList as $consumerRow){
                                    ?>
                                    <tr>
                                        <td><?php echo $consumerRow['consumer_id']?></td>
                                        <td><?php echo $consumerRow['consumer_lname'].",".$consumerRow['consumer_fname']." ".$consumerRow['consumer_mi'];?>
                                        </td>
                                        <td><?php echo $consumerRow['consumer_email']?></td>
                                        <td><?php echo $consumerRow['consumer_address']?></td>
                                        <td><?php echo $consumerRow['consumer_contact']?></td>
                                        <td><?php echo $consumerRow['consumer_dateCreated']?></td>
                                        <td><?php echo $consumerRow['consumer_status']?></td>
                                        <td>
                                            <button type="button" class="btn btn-danger">Deactivate</button>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                     ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="meatshop-tab"
                        style="margin-top:1em !important;">
                        <h2>Meatshop</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover display nowrap" id="meatshopDataTable"
                                style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($meatshopList as $meatshopRow){
                                    ?>
                                    <tr>
                                        <td><?php echo $meatshopRow['ms_id']?></td>
                                        <td><?php echo $meatshopRow['ms_companyname'];?>
                                        </td>
                                        <td><?php echo $meatshopRow['ms_email']?></td>
                                        <td><?php echo $meatshopRow['ms_address']?></td>
                                        <td><?php echo $meatshopRow['ms_contact']?></td>
                                        <td><?php echo $meatshopRow['ms_dateCreated']?></td>
                                        <td><?php echo $meatshopRow['ms_status']?></td>
                                        <td>
                                            <button type="button" class="btn btn-danger">Deactivate</button>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                     ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </main>

    <!-- modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Recipient:</label>
                            <input type="text" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Message:</label>
                            <textarea class="form-control" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>
    <!--  -->
    <footer class="footer">
        <div class="container">
            <span class="text">Copyright © 2019 Meatup All rights reserved. </span>
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Bootstrap Core JS -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- DataTable JS -->
    <script src="../assets/js/jquery.dataTables.min.js"></script>
    <script src="../assets/js/dataTables.buttons.min.js"></script>
    <script src="../assets/js/buttons.print.min.js"></script>
    <script src="../assets/js/buttons.flash.min.js"></script>
    <script src="../assets/js/buttons.html5.min.js"></script>
    <script src="../assets/js/jszip.min.js"></script>
    <script src="../assets/js/pdfmake.min.js"></script>
    <script src="../assets/js/vfs_fonts.js"></script>
    <!-- script -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#consumerDataTable').DataTable({
                "pageLength": 20,
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'print',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#meatshopDataTable').DataTable({
                "pageLength": 20,
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'print',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script>

</body>

</html>

<?php
}
?>