<?php
include '../model/getAllRecordModel.php';
if(!$_SESSION && $_SESSION['user_type']=="admin"){
    header("location:../login.php?message=pleaseLogin");
}
if($_SESSION['user_type']!="admin"){
    header("location:dashboard.php?message=pleaseLogin");
}
if($_SESSION['user_type']=="admin"){
    $getAllRecord = new GetAllRecord();
    $consumerList = $getAllRecord->getAllConsumer();
    $meatshopList = $getAllRecord->getAllMeatshop();
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/navbar-top-fixed.css">
    <link rel="stylesheet" href="../assets/css/sticky-footer.css">
    <!-- <link rel="stylesheet" href="../assets/css/dashboard.css"> -->
    <!-- Datatable -->
    <link rel="stylesheet" href="../assets/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../assets/css/buttons.dataTables.min.css">
    <!-- fontaswesom -->
    <link rel="stylesheet" href="../assets/fontawesome/css/all.css">
    <!-- custom -->
    <link rel="stylesheet" href="../assets/css/custom-style.css">
    <title>MeatUp</title>
    <style>

    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: maroon !important;">
        <a class="navbar-brand" href="#">
            <img src="../image/meatup_logo2.png" width="30" height="30" class="d-inline-block align-top" alt="">
            MeatUp
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <?php
     if($_SESSION['user_type']=="admin"){
    ?>
                <li class="nav-item active">
                    <a class="nav-link" href="admin_dashboard.php"> <i class="fas fa-columns"></i>
                        Dashboard <span class="sr-only">(current)</span></a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin_reports.php">
                        <i class="fas fa-chart-bar"></i>
                        Reports
                    </a>
                </li>
                <?php
                            }
                        ?>
                <?php
                        if($_SESSION['user_type']=="consumer"){
                        ?>
                <li class="nav-item">
                    <a class="nav-link active" href="meatshop_dashboard.php">
                        <i class="fas fa-columns"></i>
                        Dashboard <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_orders.php">
                        <i class="fas fa-shopping-cart"></i>
                        Orders
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_products.php">
                        <i class="fas fa-drumstick-bite"></i>
                        Products
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_ratings.php">
                        <i class="fas fa-users"></i>
                        Ratings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_reports.php">
                        <i class="fas fa-chart-bar"></i>
                        Reports
                    </a>
                </li>
            </ul>

                <?php
}
?>

            <span class="navbar-text">
                <a href="../controller/logout.php">Logout</a>
            </span>
        </div>
    </nav>



            <main class="container-fluid">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Reports</h1>
                    <div class="btn-toolbar mb-2 mb-md-0">

                    </div>
                </div>
                
                
            </main>

    <footer class="footer">
        <div class="container">
            <span class="text">Copyright © 2019 Meatup All rights reserved. </span>
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Bootstrap Core JS -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- DataTable JS -->
    <script src="../assets/js/jquery.dataTables.min.js"></script>
    <script src="../assets/js/dataTables.buttons.min.js"></script>
    <script src="../assets/js/buttons.print.min.js"></script>
    <script src="../assets/js/buttons.flash.min.js"></script>
    <script src="../assets/js/buttons.html5.min.js"></script>
    <script src="../assets/js/jszip.min.js"></script>
    <script src="../assets/js/pdfmake.min.js"></script>
    <script src="../assets/js/vfs_fonts.js"></script>
    <!-- script -->


</body>

</html>

<?php
}
?>