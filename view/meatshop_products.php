<?php
include '../model/productModel.php';
if(!$_SESSION){
    header("location:../login.php?message=pleaseLogin");
}
if($_SESSION['user_type'] != "meatshop"){
    header("location:../login.php?message=pleaseLogin");
}
$Product = new Product();
$productList = $Product->getAllProductById($_SESSION['user_id']);
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/navbar-top-fixed.css">
    <link rel="stylesheet" href="../assets/css/sticky-footer.css">
    <!-- Datatable -->
    <link rel="stylesheet" href="../assets/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../assets/css/buttons.dataTables.min.css">
    <!-- fontaswesom -->
    <link rel="stylesheet" href="../assets/fontawesome/css/all.css">

    <title>MeatUp</title>
</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: maroon !important;">
        <a class="navbar-brand" href="#">
            <img src="../image/meatup_logo2.png" width="30" height="30" class="d-inline-block align-top" alt="">
            MeatUp
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <?php
     if($_SESSION['user_type']=="admin"){
    ?>
                <li class="nav-item">
                    <a class="nav-link" href="admin_dashboard.php"> <i class="fas fa-columns"></i>
                        Dashboard <span class="sr-only">(current)</span></a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin_reports.php">
                        <i class="fas fa-chart-bar"></i>
                        Reports
                    </a>
                </li>
                <?php
                            }
                        ?>
                <?php
                        if($_SESSION['user_type']=="meatshop"){
                        ?>
                <li class="nav-item">
                    <a class="nav-link " href="meatshop_dashboard.php?message=">
                        <i class="fas fa-columns"></i>
                        Home <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_orders.php?message=">
                        <i class="fas fa-shopping-cart"></i>
                        Orders
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="meatshop_products.php?message=">
                        <i class="fas fa-drumstick-bite"></i>
                        Products
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_ratings.php?message=">
                        <i class="fas fa-users"></i>
                        Ratings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meatshop_reports.php?message=">
                        <i class="fas fa-chart-bar"></i>
                        Reports
                    </a>
                </li>
            </ul>
            <?php
}
?>
            <?php
                        if($_SESSION['user_type']=="consumer"){
                        ?>
            <li class="nav-item">
                <a class="nav-link active" href="consumer_dashboard.php">
                    <i class="fas fa-columns"></i>
                    Home <span class="sr-only">(current)</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="consumer_meatshopList.php">
                    <i class="fas fa-drumstick-bite"></i>
                    Meatshops
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="consumer_cart.php">
                    <i class="fas fa-shopping-cart"></i>
                    Cart
                </a>
            </li>
            </ul>
            <?php
}
?>

            <span class="navbar-text">
                <a href="../controller/logout.php">Logout</a>
            </span>
        </div>
    </nav>

    <main role="main" class="container-fluid ">
        <div class="row container-fluid">
            <div class="col-4 card shadow ">
                <form action="">
                    <div class="form-row">
                        <div class="form-group  col-md-12">
                            <!--  -->
                            <?php
                        if($_GET['message']=="missing_fields"){
                    ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php
                        }
                    ?>
                            <?php
                        if($_GET['message']=="success"){
                    ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Holy guacamole!</strong>Succesfully Added.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php
                        }
                    ?>
                            <?php
                        if($_GET['message']=="error"){
                    ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Holy guacamole!</strong> An error occured
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php
                        }
                    ?>
                            <!--  -->
                        </div>
                        <div class=" form-group col-md-12 ">
                            <h4 class="display-4">Add Product</h4>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="file" required class="form-control" id="inputGroupFile01" accept="image/*"
                                onchange="loadFile(event)" aria-describedby="inputGroupFileAddon01">
                            <img id="output" class="img-fluid center"
                                style="  display: block;  margin-left: auto;  margin-right: auto;  width: 50%;" />
                        </div>
                        <div class="form-group  col-md-12">
                            <label for="prod_name">Product name</label>
                            <input type="text" required class="form-control" id="prod_name" name="prod_name">
                        </div>
                        <div class="form-group  col-md-12">
                            <label for="prod_category">Product Category</label>
                            <input type="text" required class="form-control" id="prod_category" name="prod_category">
                        </div>
                        <div class="form-group  col-md-12">
                            <label for="prod_stock">Product Stock</label>
                            <input type="text" required class="form-control" id="prod_stock" name="prod_stock">
                        </div>
                        <div class="form-group  col-md-12">
                            <label for="prod_price">Product Price</label>
                            <input type="text" required class="form-control" id="prod_price" name="prod_price">
                        </div>
                        <div class="form-group  col-md-12">
                            <input class="btn btn-secondary" type="submit" value="Add Product" name="btnAddProduct"
                                id="btnAddProduct">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-8">
                <h2 class="display-4">Product List</h2>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover display nowrap" id="meatshopListDataTable"
                        style="width:100%">
                        <thead>
                            <th>#</th>
                            <th>Product name</th>
                            <th>Category</th>
                            <th>Stock</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                        <?php
                                    foreach($productList as $productRow){
                                    ?>
                                    <tr>
                                        <td><?php echo $productRow['prod_id']?></td>
                                        <td><?php echo $productRow['prod_name'];?></td>
                                        <td><?php echo $productRow['prod_category']?></td>
                                        <td><?php echo $productRow['prod_stock']?></td>
                                        <td><?php echo $productRow['prod_price']?></td>
                                        <td><?php echo $productRow['prod_status']?></td>
                                        <td>
                                            <button type="button" class="btn btn-danger">Deactivate</button>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                     ?>
                        </tbody>
                        <tfoot>
                        <th>#</th>
                            <th>Product name</th>
                            <th>Category</th>
                            <th>Stock</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </main>


    <footer class="footer">
        <div class="container">
            <span class="text">Copyright © 2019 Meatup All rights reserved. </span>
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Bootstrap Core JS -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script>
        var loadFile = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
        <script src="../assets/js/jquery.dataTables.min.js"></script>
    <script src="../assets/js/dataTables.buttons.min.js"></script>
    <script src="../assets/js/buttons.print.min.js"></script>
    <script src="../assets/js/buttons.flash.min.js"></script>
    <script src="../assets/js/buttons.html5.min.js"></script>
    <script src="../assets/js/jszip.min.js"></script>
    <script src="../assets/js/pdfmake.min.js"></script>
    <script src="../assets/js/vfs_fonts.js"></script>
    <!-- script -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#meatshopListDataTable').DataTable({
                "pageLength": 20,
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'print',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script>
</body>

</html>